A small project used to demonstrate new features in Docker 1.13.
Specifically the files here can be used to try out swarm-mode and "docker stack deploy" command.

Questions? Ask Lars